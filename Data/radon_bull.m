function rb = radon_bull(t)
%radon_bull.m Armin Iske
%RB=RADON_BULL(T) compute radon transform of the bull's eye phantom over the
%ponits in the vector T.
%

t = abs(t);
rb = 0.5*sqrt(9 - 16*t.^2).*(t <= 3/4) - 0.75*sqrt(1 - 4*t.^2).*(t <= 0.5) + 1/8*sqrt(1 - 16*t.^2).*(t <= 0.25);
