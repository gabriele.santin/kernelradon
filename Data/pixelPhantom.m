function ph = pixelPhantom(K, n, d)
% PIXELPHANTOM Creates a phantom made of white squares.
    ph = zeros(K);
    rng(K + n + d)
    x = d + ceil(rand(1, n) * (K - 2 * d));
    y = d + ceil(rand(1, n) * (K - 2 * d));
    for j = 1 : n
        ph(x(j) - d : x(j) + d, y(j) - d : y(j) + d) = 1;
    end
end