function R = radon_shepp_logan(t, theta)
% Comes from
%   Peter Toft: "The Radon Transform - Theory and Implementation", Ph.D. thesis.
%   Department of Mathematical Modelling, Technical University of Denmark, June 1996. 326 pages.


%         A    a     b    x0    y0    phi
%        ---------------------------------
e =    [  1   .69   .92    0     0     0
    -.8  .6624 .8740   0  -.0184   0
    -.2  .1100 .3100  .22    0    -18
    -.2  .1600 .4100 -.22    0     18
    .1  .2100 .2500   0    .35    0
    .1  .0460 .0460   0    .1     0
    .1  .0460 .0460   0   -.1     0
    .1  .0460 .0230 -.08  -.605   0
    .1  .0230 .0230   0   -.606   0
    .1  .0230 .0460  .06  -.605   0   ];

n = size(t, 1);
if size(theta, 1) ~= n
    error('t and theta must be of the same size')
end

R = zeros(n, 1);

for j = 1 : size(e, 1)
    arg = (t - e(j, 4) * cos(theta) - e(j, 5) * sin(theta)) .^ 2;
        
    t_0 = (e(j, 2) * cos(theta - e(j, 6))) .^ 2 + (e(j, 3) * sin(theta - e(j, 6))) .^ 2;
        
    ind = arg <= t_0 ;
    
    R(ind) = R(ind) + 2 * e(j, 1) * e(j, 2)* e(j, 3) * sqrt(t_0(ind) - arg(ind)) ./ t_0(ind);
end


end
