function ph = shepp_logan(N, M)
% Comes from 
%   Peter Toft: "The Radon Transform - Theory and Implementation", Ph.D. thesis.
%   Department of Mathematical Modelling, Technical University of Denmark, June 1996. 326 pages.


%         A    a     b    x0    y0    phi
%        ---------------------------------
e =    [  1   .69   .92    0     0     0   
        -.8  .6624 .8740   0  -.0184   0
        -.2  .1100 .3100  .22    0    -18
        -.2  .1600 .4100 -.22    0     18
         .1  .2100 .2500   0    .35    0
         .1  .0460 .0460   0    .1     0
         .1  .0460 .0460   0   -.1     0
         .1  .0460 .0230 -.08  -.605   0 
         .1  .0230 .0230   0   -.606   0
         .1  .0230 .0460  .06  -.605   0   ];

[x, y] = ndgrid(linspace(-1, 1, N), linspace(-1, 1, M));

ph = zeros(N, M);

for j = 1 : size(e, 1)
   xx = x - e(j, 4);
   yy = y - e(j, 5);
   cphi = cos(e(j, 6) * pi / 180);
   sphi = sin(e(j, 6) * pi / 180);
   ind = ((xx * cphi + yy * sphi) / e(j, 2)) .^ 2 + ...
       ((xx * sphi - yy * cphi) / e(j, 3)) .^ 2 <= 1;
   ph(ind) = ph(ind) + e(j, 1); 
end

ph(ph < 0) = 0;

end
