function ph=bulleye(N)
%BULLEYE Create Bull's Eye phantom.
%   P=BULLEYE(N) generate an image of the Bull's Eye phantom. 
%   P is a grayscale intensity image that consists of three concentric
%   circles. N is a scalar that specifies the number of rows and columns in P.
%   If you omit the argument, N defaults to 256.
%
%   Example
%   -------
%        ph = bulleye(256);
%        figure, imshow(ph)
%
%    See also phantom, phantom_2circ, radon_bull, back_projection, gui_inv_radon 

if nargin<1, N=256; end

x=linspace(-1,1,N);
y=x;
[X,Y] = meshgrid(x,y);
R=X.^2 + Y.^2;

ph= (R <= 9/16) - 0.75*(R <= 1/4) + 0.25*(R <= 1/16);

