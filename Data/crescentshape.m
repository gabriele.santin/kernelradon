function ph=crescentshape(N)
%CRESCENTSHAPE Create the so called 'crescent shape' phantom.
%   P=CRESCENTSHAPE(N) generate an image of a phantom. 
%   P is a grayscale intensity image that consists of 2 non-concentric
%   circles. N is a scalar that specifies the number of rows and columns in P.
%   If you omit the argument, N defaults to 256.
%
%   Example
%   -------
%        ph = phantom_2circ(256);
%        figure, imshow(ph)
%
%    See also phantom, bulleye, radon_circ, back_projection, gui_inv_radon

if nargin<1, N=256; end

c=1/8;
r1=1/2;
r2=3/8;

x=linspace(-1,1,N);
y=x;
[X,Y] = meshgrid(x,y);
R1 = X.^2 + Y.^2;
R2 = (X-c).^2 + Y.^2;

ph= (R1 <= r1^2).*(R2 > r2^2) + 0.5*(R2 <= r2^2);

end
