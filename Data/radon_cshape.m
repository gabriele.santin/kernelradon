function r=radon_cshape(t,theta)
%RB=RADON_CSHAPE(T,THETA) compute radon transform of the 'crescent shape' phantom over the
%ponits (T,THETA)
%
%   see also crescentshape, bulleye, radon_bull

c=1/8;
r1=1/2;
r2=3/8;

tcos=t-c*cos(theta);
delta=r2^2-(tcos).^2;

case1 = abs(t) <= r1;
case2 = abs(tcos) <= r2;

r=2*sqrt(r1^2-t.^2).*(case1)-sqrt(delta).*case2;
