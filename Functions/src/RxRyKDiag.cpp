#include "mex.h"
#include <math.h> 
#include "matrix.h"

static double RxRyK(double*, double*, double*, double*, double, double);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
	
    //Input
	mwSize M;
	M = mxGetM(prhs[0]);
	double ep, nu, *x;
	x = mxGetPr(prhs[0]);
	ep = *(double*) mxGetPr(prhs[1]);
	nu = *(double*) mxGetPr(prhs[2]);	
	ep = pow(ep, 2);
    nu = pow(nu, 2);
    
    //Output
	plhs[0] = mxCreateDoubleMatrix(M, 1, mxREAL);
	double *A = mxGetPr(plhs[0]);

	//Computation
	mwSize col;	
    for(col = 0; col < M; col++){
		*(A + col) = RxRyK(x + col, x + M + col, x + col, x + M + col, ep, nu);
	}
	return;
}

static double RxRyK(double *x1, double *x2, double *y1, double *y2, double ep, double nu){
	double c = cos(*x2 - *y2), h, R;
	h = 2 * (pow(ep + nu, 2) - pow(ep, 2) * pow(c, 2));
	R = - 2 * (pow(nu, 2) + 2 * ep * nu) * ((ep + nu) * (pow(*x1, 2) + pow(*y1, 2)) - 2 * ep * (*x1) * (*y1) * c);
	return M_PI * sqrt(2. / h) * exp(R / h);
}



