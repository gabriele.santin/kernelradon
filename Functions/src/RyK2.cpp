#include"mex.h"
#include <math.h> 
#include "matrix.h"

static double alpha(double*, double*, double*, double*, double, double);

static double RyK(double*, double*, double*, double*, double, double);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
    //Input sol = RyK(x, y, ep, nu, c);
	mwSize M, N;
	M = mxGetM(prhs[0]);
    N = mxGetM(prhs[1]);
    
    double *x, *y, *c, ep, nu;
	x = mxGetPr(prhs[0]);
	y = mxGetPr(prhs[1]);
    c = mxGetPr(prhs[4]);
    ep = *((double*) mxGetPr(prhs[2]));
	nu = *((double*) mxGetPr(prhs[3]));
    ep = pow(ep, 2);
    nu = pow(nu, 2);
	
    //Output
	plhs[0] = mxCreateDoubleMatrix(M, 1, mxREAL);
	double *v = mxGetPr(plhs[0]);

    //Computations
   	mwSize row, col;	
    for(row = 0; row < M; row++){
        for(col = 0; col < N; col++){
            *(v + row) += RyK(x + row, x + M + row, y + col, y + N + col, ep, nu) * (*(c + col));
//             *(v + row + M * col) += *(x + row) * (*(y + col));
        }
    }    

    return;
}

static double alpha(double *x1, double *x2, double *t, double *th, double ep, double nu){
// 	double nrmX2, x_inner_nth;
// 	nrmX2 = nu * (nu + 2 * ep) * (pow(*x1, 2) + pow(*x2, 2));
// 	x_inner_nth = (*x1) * cos(*th) + (*x2) * sin(*th);
// 	return -(nrmX2 + pow(ep, 2) / (ep + nu) * pow((ep + nu) / ep * (*t) - x_inner_nth, 2)) / (ep + nu);
	double nrmX2, x_inner_nth, x_inner_ntho;
	nrmX2 = pow(*x1, 2) + pow(*x2, 2);
	x_inner_nth = (*x1) * cos(*th) + (*x2) * sin(*th);
   	x_inner_ntho = -(*x1) * sin(*th) + (*x2) * cos(*th);
	return -(ep + nu) * (nrmX2 + pow(*t, 2)) + 2 * ep * (*t) * x_inner_nth + pow(ep, 2) / (ep + nu) * pow(x_inner_ntho, 2);
}

static double RyK(double *x1, double *x2, double *t, double *th, double ep, double nu){
	return sqrt(M_PI / (ep + nu)) * exp(alpha(x1, x2, t, th, ep, nu));
}

