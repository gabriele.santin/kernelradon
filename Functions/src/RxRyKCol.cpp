#include"mex.h"
#include <math.h> 
#include "matrix.h"

static double RxRyK(double*, double*, double*, double*, double, double);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
        
    //Input
	mwSize M;
	M = mxGetM(prhs[0]);
    double *x, *y, ep, nu;
	x = mxGetPr(prhs[0]);
	y = mxGetPr(prhs[1]);
    ep = *((double*) mxGetPr(prhs[2]));
	nu = *((double*) mxGetPr(prhs[3]));
	ep = pow(ep, 2);
    nu = pow(nu, 2);
    
    //Output
	plhs[0] = mxCreateDoubleMatrix(M, 1, mxREAL);
	double *v = mxGetPr(plhs[0]);

	//Computation
	mwSize row;	
    for(row = 0; row < M; row++){
        *(v + row) = RxRyK(x + row, x + M + row, y, y + 1, ep, nu);
	}
	return;
}

static double RxRyK(double *x1, double *x2, double *y1, double *y2, double ep, double nu){
	double c = cos(*x2 - *y2), h, R;
	h = 2 * (pow(ep + nu, 2) - pow(ep, 2) * pow(c, 2));
	R = - 2 * (pow(nu, 2) + 2 * ep * nu) * ((ep + nu) * (pow(*x1, 2) + pow(*y1, 2)) - 2 * ep * (*x1) * (*y1) * c);
	return M_PI * sqrt(2. / h) * exp(R / h);
}








