#include "Eigen/Dense"
#include "Eigen/Core"
#include "mex.h"
#include <math.h> 
using namespace Eigen;

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]){
    //Input
	int M, N;
	M = (int) mxGetM(prhs[0]);
    N = (int) mxGetM(prhs[1]);
    
	Map<MatrixXd> x((double*) mxGetPr(prhs[0]), M, 2), y((double*) mxGetPr(prhs[1]), N, 2), co((double*) mxGetPr(prhs[4]), N, 1);
	double ep, nu;
	ep = *((double*) mxGetPr(prhs[2]));
	nu = *((double*) mxGetPr(prhs[3]));
    ep = pow(ep, 2);
    nu = pow(nu, 2);
	
    //Computations
	MatrixXd sol = MatrixXd::Zero(M, 1), R = MatrixXd::Zero(M, 1), Nrm =- (ep + nu) * x.rowwise().squaredNorm();
	double s, c;
	co *= sqrt(M_PI / (ep + nu));
	for(int j = 0; j < N; j++){
		s = sin(y(j, 1));
        c = cos(y(j, 1));
		R = Nrm - (ep + nu) * pow(y(j, 0), 2) * (MatrixXd::Constant(M, 1, 1)) + (pow(ep, 2) / (ep + nu)) * (x.col(0) * s - x.col(1) * c).cwiseProduct(x.col(0) * s - x.col(1) * c) + 2 * ep * y(j, 0) * (x.col(0) * c + x.col(1) * s);
		sol += co(j) * R.unaryExpr(std::ptr_fun((double (*)(double))exp));
	}
	
	//Output
	plhs[0] = mxCreateDoubleMatrix(M, 1, mxREAL);
	memcpy(mxGetPr(plhs[0]), sol.data(), M * sizeof(double));
	
    return;
}

