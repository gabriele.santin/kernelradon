function [modelPar, fmax, pmax] = trainModel(R, radP, gridP, ph, valPar, trPar)

% Solve for each parameter value
Eps = linspace(valPar.minEp, valPar.maxEp, valPar.nEp);
Nus = linspace(valPar.minNu, valPar.maxNu, valPar.nNu);
errVal = zeros(valPar.nEp, valPar.nNu);
disp('Parameters validation:')
for i = 1 : valPar.nEp
    for j = 1 : valPar.nNu
        Acol = @(k, l) RxRyKCol(radP(k, :), radP(l, :), Eps(i), Nus(j));
        Adiag = @(k) RxRyKDiag(radP(k, :), Eps(i), Nus(j));
        [c, ind, n] = newtonYYDataMF(Acol, Adiag, R, valPar.tolF, valPar.tolP, valPar.nBasis, trPar.gType, 1);
        sol = RyK(gridP, radP(ind, :), Eps(i), Nus(j), c);
        errVal(i, j) = valPar.errFun(sol, ph);
        fprintf('ep = %2.2e, nu = %2.2e, error = %2.2e, n = %2d\n', Eps(i), Nus(j), errVal(i, j), n)
    end
end


% Find minimal error parameters
[tmp, ii] = min(errVal);
[~, minErrIndNu] = min(tmp);
minErrIndEp = ii(minErrIndNu);
epOpt = Eps(minErrIndEp);
nuOpt = Nus(minErrIndNu);
modelPar.ep = epOpt;
modelPar.nu = nuOpt;

% Solve the problem
disp('Training:')
Acol = @(i, j) RxRyKCol(radP(i, :), radP(j, :), modelPar.ep, modelPar.nu);
Adiag = @(i) RxRyKDiag(radP(i, :), modelPar.ep, modelPar.nu);
[modelPar.c, modelPar.ind, modelPar.n, ~, ~, fmax, pmax] = ...
    newtonYYDataMF(Acol, Adiag, R, trPar.tolF, trPar.tolP, trPar.nBasis, trPar.gType, 1);

end


