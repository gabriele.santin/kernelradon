function [R, p] = getData(par)
% GETDATA Returns a Radon transform and a set of Radon points.
%   [R, p] = GETDATA(phType, data, K, N, M) returns the M x N Radon 
%   transform R of the K x K phantom of type phType and the set of N * M 
%   Radon points p = [t, theta] used to compute R. The points are in 
%   parallel beam geometry with N angles and M lines per angle if 
%   data == 'parallel' or N * M scattered lines if data == 'scattered'.
%
%   The parallel beam geometry Radon transform is computed with the
%       AIRtools package: http://www2.compute.dtu.dk/~pcha/AIRtools/
%   and the scattered Radon transform with code written by Amos Sironi.
%


phType = par.phType;
geo = par.geo;
K = par.K;
N = par.N;
M = par.M;

ph = getPhantom(par);

if strcmp(geo, 'scattered')
    THETA = rand(M * N, 1) * pi;
    T = rand(M * N, 1) * 2 * sqrt(2) - sqrt(2);
    [T, i] = sort(T);
    THETA = THETA(i);
    if strcmp(phType, 'cshape')
        R = radon_cshape(T,THETA);
    elseif strcmp(phType, 'bull')
        R = radon_bull(T); 
    elseif strcmp(phType, 'shepp_logan')
        R = radon_shepp_logan(T, THETA); 
    else
        error('Scattered data available only for phantom "cshape", "bull", "shepp_logan"')
    end
elseif strcmp(geo, 'parallel')
    theta = (0 : N-1) * pi / N;
    t = linspace(-sqrt(2), sqrt(2), M);  
    [THETA, T] = meshgrid(theta, t);
    A = paralleltomo(K, theta / pi * 180, M);
    R = A * ph(:);
else
    error(['Data "' geo '" not available'])
end

p = [T(:) THETA(:)];
R = R / max(R(:));

end
