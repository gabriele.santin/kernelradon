% RYK Evaluates the kernel - Radon interpolant.
%   sol = RYK(gridP, K, radP, ep, nu, c) evaluates on the K * K points 
%   gridP the kernel - Radon interpolant defined by coefficients c, centers 
%   radP and with shape parameters ep, nu. 
