function [ph, p] = getPhantom(par)
% GETPHANTOM Returns a phantom and the grid where it is defined.
%   [ph, p] = GETPHANTOM(phType, K, box) returns the K x K square segment 
%   of the phantom ph of type phType contained in the box b, and the K * K
%   points p where it is defined. 
%

phType = par.phType;
K = par.K;

if ~isfield(par, 'box') || isempty(par.box)
    KK = K;
    b = [-1 1 -1 1];
else
    b = par.box;
    dx = b(2) - b(1);
    dy = b(4) - b(3);
    if dx < dy
        b(2) = b(1) + dy;
    else
        b(4) = b(3) + dx;
    end
    KK = ceil(2 / dx) * K;
end

switch phType
    case 'cshape',
        ph = crescentshape(KK);
    case 'bull',
        ph = bulleye(KK);
    case 'square',
        ph = pixelPhantom(KK, 1, 1);
    case 'shepp_logan',
        ph = shepp_logan(KK, KK);
    otherwise
        error(['Phantom "' phType '" not available'])
end

if nargout == 2
    [x1, x2] = meshgrid(linspace(-1, 1, KK));
    x1 = x1(:); 
    x2 = x2(:); 
    ind = b(1) <= x1 & x1 <= b(2) & b(3) <= x2 & x2 <= b(4);
    p = [x1(ind) x2(ind)];
    ph = ph(ind);
    K = sqrt(length(p));
    ph = reshape(ph, K, K);
end

end