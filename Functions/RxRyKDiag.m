% RXRYKDIAG Returns the diagonal of the kernel - Radon matrix.
%   v = RXRYKDIAG(x, ep, nu); returns the diagonal of the kernel - Radon
%   matrix corresponding to the centers x and with shape parameters ep, nu.

