% RXRYKCOL Returns a column of the kernel - Radon matrix.
%   v = RXRYKCOL(x, y, ep, nu) returns the column of the kernel - Radon
%   matrix corresponding to the center x and the vector of points y and
%   with shape parameters ep, nu.