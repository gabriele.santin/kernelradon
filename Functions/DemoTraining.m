%%
close all, clear all
addpath('Functions')
addpath('Data')


%% Parameters
savePath = 'Examples/Example_Test.mat';

% Data
dataPar.phType = 'bull';      % phantom type (see getPhantom.m)
dataPar.geo = 'parallel';       % data type (see getData.m)
dataPar.K = 128;                % size of reconstructed phantom
dataPar.N = 32;                % number of angles
dataPar.M = 32;                % number of lines per angle
dataPar.box = [];               % square for zoom of reconstructed phantom

% Training (Newton basis)
trPar.gType = 'fgreedy';     % type of greedy selection (f,P, fpgreedy)
trPar.tolF = 1e-12;           % residual tolerance
trPar.tolP = 1e-14;          % power function tolerance
trPar.nBasis = 200;         % max number of basis

% Validation (ep & nu)
valPar.minEp = 10;              % min value of epsilon
valPar.maxEp = 100;             % max value of epsilon
valPar.nEp = 1;                % number of values of epsilon
valPar.minNu = 0.1;             % min value of nu
valPar.maxNu = 2;               % max value of nu
valPar.nNu = 1;                % number of values of nu
valPar.tolF = 1e-8;             % residual tolerance
valPar.tolP = 1e-10;            % power function tolerance
valPar.nBasis = 1;           % max number of basis
valPar.errFun = ...             % error function
    @(sol, phantom) sqrt(sum((sol - phantom(:)) .^ 2));
% SSIM: 1 - ssim(reshape(sol, dataPar.K, []), phantom);
% RMSE:  sqrt(sum((sol - phantom(:)) .^ 2));


%% Generation of the data
fprintf('Generation of the data\n')
[R, radP] = getData(dataPar);


%% Generation of the reconstruction grid
fprintf('Generation of the reconstruction grid\n')
[ph, gridP] = getPhantom(dataPar);


%% Training of the model
fprintf('Training of the model\n')
[modelPar, fmax, pmax] = trainModelNS(R, radP, gridP, ph, valPar, trPar);


% %%
% if exist('savePath', 'var') && ~isempty(savePath)
%     save(savePath, 'dataPar', 'trPar', 'valPar', 'modelPar');
%     fprintf('Model saved\n')
% end


%% Reconstruction on the grid
fprintf('Reconstruction on the grid\n')
sol = RyK(gridP, radP(modelPar.ind, :), modelPar.ep, modelPar.nu, modelPar.c);
sol = reshape(sol, dataPar.K, dataPar.K);
sol = flipud(sol);


%% Computation of the error
err = abs(sol - ph);
maxerr = max(err(:));
rmse = sqrt(sum(err(:) .^ 2)) / dataPar.K;
ssimerr = ssim(sol, ph);
fprintf('RMSE = %2.2e, MaxErr = %2.2e, SSIM = %2.2e\n', rmse, maxerr, ssimerr)


%% Plotting
p = zeros(dataPar.M, dataPar.N); p(modelPar.ind) = 1;
f1 = figure(1);
colormap(gray) %'default'),
subplot(2,2,1), imagesc(gridP(:, 1), gridP(:, 2), ph), title('Original Phantom')
subplot(2,2,2), imagesc(gridP(:, 1), gridP(:, 2), sol), title('Reconstructed phantom')
subplot(2,2,3), imagesc(radP(:, 1), radP(:, 2), reshape(R, dataPar.M, [])'),
xlabel('t'), ylabel('theta')
title('Sinogram')
subplot(2,2,4), imagesc(radP(:, 1), radP(:, 2), p'),
xlabel('t'), ylabel('theta')
title('Selected Radon points / lines')

f2 = figure(2);
semilogy(pmax), title('Decay of the power function L_{\infty}')
f3 = figure(3);
semilogy(fmax), title('Decay of the residual in L_{\infty}')



