function f = plotLines(t, th, fig)

s = [-2, 2]';
cth = cos(th);
sth = sin(th);
x = bsxfun(@minus, (t .* cth)', bsxfun(@times, s, sth'));
y = bsxfun(@plus, (t .* sth)', bsxfun(@times, s, cth'));
f = figure(fig);
colormap(gray)
line(x, y, 'color', 'k')
xlabel('x')
ylabel('y')
set(gca, 'fontsize', 16)
grid on

end
