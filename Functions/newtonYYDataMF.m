function [a, indI, n, c, Cut, fmax, pmax] = newtonYYDataMF(Acol, Adiag, f, tolF, tolP, maxIter, gType, KScale)

% size of data
[N, q] = size(f);
maxIter = min(maxIter, N);

if ~any(strcmpi(gType, {'fgreedy', 'fpgreedy', 'pgreedy'}))
    error('...')
end

indI = zeros(maxIter, 1); % selected indexes
notIndI = (1 : N)'; % not yet selected indexes
Vx = zeros(N, maxIter); % values of the Newton basis on the points
c = zeros(maxIter, q); % coefficients of the interpolant/expansion
fmax = zeros(maxIter, 1); % maximal residuals
pmax = zeros(maxIter, 1); % maximal power function on training set
p = Adiag(1 : N); % power function squared (assuming transl. inv. ker.)
Cut = zeros(maxIter); % matrix of change of basis

for n = 1 : maxIter
    % select the current index
    if strcmp(gType, 'fgreedy')
        [fmax(n), i] = max(abs(f(notIndI, :)) * KScale);
        pmax(n) = max(p(notIndI));
    elseif strcmp(gType, 'fpgreedy')
        [~, i] = max(sum(abs(bsxfun(@rdivide, f(notIndI, :), sqrt(p(notIndI)))), 2));
        fmax(n) = max(abs(sum(f(notIndI, :), 2)));
        pmax(n) = max(p(notIndI));
    else
        [pmax(n), i] = max(p(notIndI));
        fmax(n) = max(sum(abs(f(notIndI, :)), 2));
    end
    
    % add the current index
    indI(n) = notIndI(i);
    
    if fmax(n) >= tolF && pmax(n) >= tolP
        % compute the nth basis
        Vx(notIndI, n) = Acol(notIndI, indI(n)) - Vx(notIndI, 1 : n - 1) * Vx(indI(n), 1 : n - 1)';
        % normalize the nth basis
        Vx(notIndI, n) = Vx(notIndI, n) / sqrt(p(indI(n)));
        % compute the nth coefficient
        c(n, :) = f(indI(n), :) ./ sqrt(p(indI(n)));
        % update the change of basis
        Cut(n, 1 : n) = [-Vx(indI(n), 1 : n - 1) * Cut(1 : n - 1, 1 : n - 1),  1] / Vx(indI(n), n);
        % update the power function
        p(notIndI) = p(notIndI) - Vx(notIndI, n) .^ 2;
        % update the residual
        f(notIndI, :) = f(notIndI, :) - bsxfun(@times, Vx(notIndI, n), c(n, :));
        % remove the nth index from the dictionary
        notIndI = notIndI([1 : i - 1, i + 1 : end]);
    else
        n = n - 1;
        break
    end
end

% Final resize
c = c(1 : n, :);
Cut = Cut(1 : n, 1 : n);
a = Cut' * c;
indI = indI(1 : n);
fmax = fmax(1 : n);
pmax = pmax(1 : n);

end
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       