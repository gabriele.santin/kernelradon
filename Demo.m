%%
close all, clear all
addpath('Functions')
addpath('Data')

%% Parameters
plot = 1;
loadPath = 'Examples/bull_paper.mat';
% 'slogan_scattered_paper.mat'; %'cshape_scattered_paper.mat'; %

compareFBP = 1;

%% Data and model loading
fprintf('Data and model loading\n')
if exist('loadPath', 'var') && ~isempty(loadPath)
    load(loadPath);
    disp('Model loaded')
else
    error('Data not found')
end

%% Generation of the data
fprintf('Generation of the data\n')
if isfield(dataPar, 'R') && isfield(dataPar, 'radP')
    R = dataPar.R;
    radP = dataPar.radP;
else    
    [R, radP] = getData(dataPar);
end

if compareFBP
    dataPar.K = 90;
end

%% Generation of the reconstruction grid
fprintf('Generation of the reconstruction grid\n')
[ph, gridP] = getPhantom(dataPar);


%% Reconstruction on the grid
fprintf('Reconstruction on the grid\n')
tic
sol = RyK(gridP, radP(modelPar.ind, :), modelPar.ep, modelPar.nu, modelPar.c);
toc
sol = reshape(sol, dataPar.K, []);
sol = flipud(sol);


%% Computation of the error
if ~exist('ssim', 'builtin')    
    ssim = @(x, y) NaN;
    warning('The ssim function is not available. A placeholder result (NaN) is used')
end
if ~exist('psnr', 'builtin')    
    psnr = @(x, y, z) NaN;
    warning('The psnr function is not available. A placeholder result (NaN) is used')
end

err = abs(sol - ph);
maxerr = max(err(:));
mse = sum(err(:) .^ 2) / length(err(:));
ssimerr = ssim(sol, ph);
psnrerr = psnr(sol, ph, 255);
fprintf('MSE = %2.2e, MaxErr = %2.2e, SSIM = %2.2e, PSNR = %2.2e\n', mse, maxerr, ssimerr, psnrerr)


%% Plotting Radon
p = zeros(dataPar.M, dataPar.N); p(modelPar.ind) = 1;
f1 = figure(1);
colormap(gray) %'default'),
subplot(2,2,1), imagesc(gridP(:, 1), gridP(:, 2), ph), title('Original Phantom')
subplot(2,2,2), imagesc(gridP(:, 1), gridP(:, 2), sol), title('Reconstructed phantom')
subplot(2,2,3), imagesc(radP(:, 1), radP(:, 2), reshape(R, dataPar.M, [])'),
xlabel('t'), ylabel('theta')
title('Sinogram')
subplot(2,2,4), imagesc(radP(:, 1), radP(:, 2), p'),
xlabel('t'), ylabel('theta')
title('Selected Radon points / lines')

%% Plotting Radon
if ~exist('iradon', 'builtin')
    compareFBP = 0;
    warning('Matlab builin Radon transform is not available. Skipping comparison with FBP.')
end

if compareFBP
    r = reshape(R', 128, 128);
    N = dataPar.N;
    ttt = (0 : N-1) * pi / N /pi * 180;
    
    I = iradon(r, ttt', 'linear', 'ram-lak', 1);
    subplot(2,2,1), imagesc(gridP(:, 1), gridP(:, 2), ph), title('Original Phantom')
    subplot(2,2,2), imagesc(gridP(:, 1), gridP(:, 2), I), title('Reconstructed phantom FBP')
    subplot(2,2,3), imagesc(radP(:, 1), radP(:, 2), reshape(R, dataPar.M, [])'), 
    xlabel('t'), ylabel('theta')
    title('Sinogram')
    

    % Computation of the error
    err = abs(I - ph);
    maxerr = max(err(:));
    mse = sum(err(:) .^ 2) / length(err(:));
    ssimerr = ssim(I, ph);
    psnrerr = psnr(I, ph, 255);
    fprintf('MSE = %2.2e, MaxErr = %2.2e, SSIM = %2.2e, PSNR = %2.2e\n', mse, maxerr, ssimerr, psnrerr)

end
