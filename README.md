# Kernel Radon
Matlab implementation of the algorithm of "Image reconstruction from scattered Radon data by weighted positive definite kernel functions".

## Usage
Note for use:
* DemoTraining.m trains a new model
* Demo.m runs a previously trained model
* Pre-trained models are saved in Examples/
* Parts of the code are written in C++/mex and need to be compiled. 
* To compile some function, the [Eigen](http://eigen.tuxfamily.org/) library is required.
* Pre-compiled functions for Linux platforms are provided.


## Literature
If you use this code in your work, please consider citing the paper

Article (DeMarchi2018) 


> De Marchi, S.; Iske, A. & Santin, G. [Image reconstruction from scattered Radon data by weighted positive definite kernel functions](https://link.springer.com/article/10.1007%2Fs10092-018-0247-6) Calcolo, 2018, 55, 2

```bibtex:
@Article{DeMarchi2018,
  Title                    = {Image reconstruction from scattered Radon data by weighted positive definite kernel functions},
  Author                   = {De Marchi, S. and Iske, A. and Santin, G.},
  Journal                  = {Calcolo},
  Year                     = {2018},
  Month                    = {Feb},
  Number                   = {1},
  Pages                    = {2},
  Volume                   = {55},
  Day                      = {02},
  Doi                      = {10.1007/s10092-018-0247-6},
  ISSN                     = {1126-5434},
  Url                      = {https://doi.org/10.1007/s10092-018-0247-6}
}

```


## Author
This code have been developed and is maintained by (me) 

```
Gabriele Santin
gsantin@fbk.eu
Researcher at MobS - Mobile and Social Computing Lab
Center for Information and Communication Technology
Fondazione Bruno Kessler
https://gabrielesantin.github.io/
Via Sommarive, 18
I-38123, Trento, Ital
```
Please contact me for any question.

